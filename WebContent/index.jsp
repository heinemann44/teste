<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="ISO-8859-1">
</head>
<body>
<jsp:include page="/listaProdutosServlet" />
	<div>	
	Produto: 
		<select id="cbProduto" onchange="mostrarCamposAdicionais()">
		    <option value="">Valores</option>
		    <c:forEach var="produto" items="${listaProdutos}">
		        <option value="${produto.codigo}">
		            ${produto.codigo}
		        </option>
		     </c:forEach>
		</select>		
	Custo/Pre�o: 
		<input type="text" id="precoProduto" value="0.00">	
	Quantidade: 
		<input type="text" id="quantidadeProduto" value="0">	
	</br>
	</div>
	<div id="divPagamento" style="display: none;">
	Forma de pagamento: 
		<select id="cbOpcaoPagamento" onchange="mostrarTaxa()">
			<option value="">Op��o</option>
		    <option value="credito">Cr�dito</option>
		    <option value="parcelado">Parcelado</option>
		    <option value="creditoEParcelado">Cr�dito e Parcelado</option>
		    <option value="debito">D�bito</option>
		</select>
	</div>
	<div id="divTaxaCredito" style="display: none;">
	Taxa de cr�dito negociada: 
		<input type=text value="0.00" id="taxaCredito" onkeypress="verificarNumero(event)" oninput="validarTaxaCredito()" required="required" />
	</div>
	<div id="divTaxaParceladox6" style="display: none;">
	Taxa do Parcelado 6x negociada: 
		<input type="text" value="0.00" id="taxaParceladox6" oninput="validarTaxaParceladox6()" />
	</div>
	<div id="divTaxaParceladox12" style="display: none;">
	Taxa do Parcelado 12x negociada: 
		<input type="text" value="0.00" id="taxaParceladox12" />
	</div>
	<div id="divTaxaAmexCredito" style="display: none;">
	Taxa Amex Cr�dito na concorr�ncia: 
		<input type="text" id="taxaAmexCredito" onkeypress="verificarNumero(event)" oninput="validarTaxaAmexCredito()"required="required" />
	</div>
	<button type="submit" id="btnSalvar">Salvar</button>
</body>

<script type="text/javascript">

	function verificarNumero(evt){        
        var character = String.fromCharCode(evt.which);
        if(!(/[0-9]/.test(character)) && !(character == ".")){
            evt.preventDefault();
    	}
	}
	
	function mostrarCamposAdicionais() {
		codigoProduto = document.getElementById("cbProduto").value;
	    mostrarOpcoesPagamento(codigoProduto);
	    mostrarTaxa();
	}

	
	function mostrarOpcoesPagamento(codigo){
		if (codigo == 4409 || codigo == 4628){
			document.getElementById("divPagamento").style.display = 'block';
		}else{
			document.getElementById("divPagamento").style.display = 'none';
		}	
	}
	
	function mostrarTaxa(){
		mostrarTaxaCredito();
		mostrarTaxaParceladox6();
		mostrarTaxaParceladox12();
		mostrarTaxaAmexCredito();
	}
	
	function mostrarTaxaCredito(){
		codigo = document.getElementById("cbProduto").value;
		formaPagamento = document.getElementById("cbOpcaoPagamento").value; 
		if ((codigo == 4628) && (formaPagamento == "credito" || formaPagamento == "creditoEParcelado")){
			document.getElementById("divTaxaCredito").style.display = 'block';				
		}else{
			document.getElementById("divTaxaCredito").style.display = 'none';
		}
		validarTaxaCredito();
	}
		
	function validarTaxaCredito(){
		precoProduto = Number(document.getElementById("precoProduto").value);
		taxaCredito = Number(document.getElementById("taxaCredito").value);
		
		if (taxaCredito > precoProduto){
			document.getElementById("btnSalvar").disabled = false;
		}else{
			document.getElementById("btnSalvar").disabled = true;
		}
	}
	
	function mostrarTaxaParceladox6(){
		codigo = document.getElementById("cbProduto").value;
		formaPagamento = document.getElementById("cbOpcaoPagamento").value; 

		if ((codigo == 4628) && (formaPagamento == "parcelado" || formaPagamento == "creditoEParcelado")){
				document.getElementById("divTaxaParceladox6").style.display = 'block';	
		}else{
			document.getElementById("divTaxaParceladox6").style.display = 'none';
		}	
		validarTaxaParcelado();
	}
	
	function validarTaxaParcelado(){
		validarTaxaParceladox6();
		validarTaxaParceladox12();
	}
	
	function validarTaxaParceladox6(){
		precoProduto = Number(document.getElementById("precoProduto").value);
		taxaCredito = Number(document.getElementById("taxaParceladox6").value);
		
		if (taxaCredito > precoProduto){
			document.getElementById("btnSalvar").disabled = false;
		}else{
			document.getElementById("btnSalvar").disabled = true;
		}
	}
	
	function mostrarTaxaParceladox12(){
		codigo = document.getElementById("cbProduto").value;
		formaPagamento = document.getElementById("cbOpcaoPagamento").value; 

		if ((codigo == 4628) && (formaPagamento == "parcelado" || formaPagamento == "creditoEParcelado")){
				document.getElementById("divTaxaParceladox12").style.display = 'block';	
		}else{
			document.getElementById("divTaxaParceladox12").style.display = 'none';
		}	
		validarTaxaParcelado();
	}
	

	function validarTaxaParceladox12(){
		precoProduto = Number(document.getElementById("precoProduto").value);
		taxaCredito = Number(document.getElementById("taxaParceladox12").value);
		
		if (taxaCredito > precoProduto){
			document.getElementById("btnSalvar").disabled = false;
		}else{
			document.getElementById("btnSalvar").disabled = true;
		}
		completarCampoTaxaParceladox12();
	}
		
	function completarCampoTaxaParceladox12(){
		preco = document.getElementById("precoProduto").value;
		
		if (verificarEstoque()){
			document.getElementById("taxaParceladox12").disabled = false;
		}else{
			document.getElementById("taxaParceladox12").value = "N/A";
			document.getElementById("taxaParceladox12").disabled = true;
		}
	}
	
	function verificarEstoque(){
		quantidade = Number(document.getElementById("quantidadeProduto").value);
		if (quantidade > 0){
			return true;
		}else{
			return false;
		}					
	}
	
	function validarTaxaAmexCredito(){
		precoProduto = Number(document.getElementById("precoProduto").value);
		taxaAmexCredito = Number(document.getElementById("taxaAmexCredito").value);
		
		if (taxaAmexCredito < precoProduto){
			document.getElementById("btnSalvar").disabled = false;
		}else{
			document.getElementById("btnSalvar").disabled = true;
		}
	}
	
	function mostrarTaxaAmexCredito(){
		codigo = document.getElementById("cbProduto").value;
		if (codigo == 4430){
			document.getElementById("divTaxaAmexCredito").style.display = 'block';
		}else{
			document.getElementById("divTaxaAmexCredito").style.display = 'none';
		}	
	}
	

</script>
</html>