package model;
	
public class Produto {
	
	private int codigo;
	
	public Produto() {
		
	}
	

	public Produto(int codigo) {
		this.codigo = codigo;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}	

}
